[![QMS Class](https://qms.csem.local/gitlab-badge/468)](http://sps3.csem.local/divs/dive/QMS/_layouts/15/WopiFrame2.aspx?sourcedoc=/divs/dive/QMS/Procedures/EP-940%20Software%20Development.docx&action=default)

# Xemway Python

## Install

```bash
pip install git+https://gitlab.csem.local/div-e/libraries/xemway/core/xemway-python.git
```

## Usage

### Daemon

```python
#!/usr/bin/env python
from xemway import Service

class Talkative(Service):
    def init(self):
        self.logger.info('I am going to talk too much.')

    def worker(self):
        while True:
            sleep(3)
            self.logger.info('I am not yet sleeping!')

    def shutdown(self):
        self.logger.info('Unfortunately, I am dying, you will not hear me anymore.')

if __name__ == '__main__':
    cli = Talkative()
    cli()
```

#### Register the daemon with `systemd`

```bash
$ sudo python - <<EOF
from myPackage import Talkative
print(Talkative.get_service_file('/usr/local/bin/talkative'))
EOF > /etc/systemd/system/talkative.service

$ sudo systemctl enable talkative
$ sudo systemctl start talkative
```

### Messaging Tutorial

#### Pub/Sub

This is how to do a publish/subscribe communication.

```python

    class MyClass(Messaging):
        def __init__(self):
            Messaging.__init__(self, host=BROKER_ADDR, port=BROKER_PORT, credentials_file=None)
            self.connect(sync=False)
            
        @subscribe('xemway/testing/mytopic')
        def mytopic_cb(self, topic, payload):
            #  Put your topic handler code here
            
        def publish_something(self):
           self.publish('xemway/testing/mytopic', 'Hello')
            
```

#### Request/Response

An unicast communication pattern was implemented over MQTT. It's useful when you need something similar to an HTTP request.
This is how it works internally:
1.	The responder subscribes to a topic ending with `@query`, e.g. `tabede/bergamo/bmse/command/deviceid/endpointid/@query`
2.	The requester creates an ID (or key) and subscribes to the topic ending with this ID: `tabede/bergamo/bmse/command/deviceid/endpointid/dea2676b-26ca-438e-a3c6-58b24ee48597`
Notice that you could create a new temporary ID for each individual request, or reuse the same ID if you don’t send multiple requests in parallel.
3.	The requester sends its request by publishing a MQTT message to `tabede/bergamo/bmse/command/deviceid/endpointid/@query`
The message is encapsulated in a JSON, together with the request ID and a version number. E.g.:
```json
{
  "version": {
    "major": 2,
    "minor": 0
  },
  "requester_id": "dea2676b-26ca-438e-a3c6-58b24ee48597",
  "payload": {
    "command": "SET",
    "request": 1234
  }
}
```
4.	The responder gets the requester ID from the message, processes the request and sends the answer to `tabede/bergamo/bmse/command/deviceid/endpointid/dea2676b-26ca-438e-a3c6-58b24ee48597`


This request-response communication was implemented in `XEMWAY`. Here is how to use it:

```python
    class MyClass(Messaging):
        def __init__(self):
            Messaging.__init__(self, host=BROKER_ADDR, port=BROKER_PORT, credentials_file=None)
            self.connect(sync=False)
            
        # Server side code
        @query('xemway/testing/my_req_resp_topic')
        def query_cb(self, topic, command, message_body):
            #  Put your request handler code here
            response = '42'
            return response
            
        # Client side code
        def request_something(self):
           response = self.request('xemway/testing/my_req_resp_topic', 'my request', timeout=5)
           print(response)
```

#### Credentials

The username and password for the messaging broker must be stored in a simple YAML file:

```yaml
username: doe
password: foo
```
Credentials are hidden in a separated file so that you can manage different access rights for that file.

### Cli

The `cli` module provides command line options and commands through decorators. The decorator `@option` can be used either as a
method decorator, either as a class decorator.

The `cli-demo` example shows how to use the `Cli` module:

```python
from xemway import Cli, option, command


@option('-t', '--test', default=42, show_default=True, help='Answer to everything')
@option('-m', '--message', default='Hello', show_default=True, help='Greetings')
class CliDemo(Cli):
    def __init__(self):
        self.foo = 33

    @command('ping')
    def ping(self):
        print('pong')

    @option('-f', '--foo', default=33, show_default=True, help='Decorator option test 1')
    def set_foo(self, foo, **kwargs):
        if foo:
            self.foo = foo

    def main(self):
        print(self.message)
        print(self.test)
        print(self.foo)


if __name__ == '__main__':
    cli_demo = CliDemo()
    cli_demo()
```

In the example above you can see that the `@option` class decorator creates new class properties,
e.g. `self.test` and `self.message`.


### Adapter

The Xemway Adapter adds some `@option` for the Messaging class using the `Cli` module:

 ```bash
  -c, --cred TEXT     Messaging broker credentials YAML file
  -p, --port INTEGER  Messaging port broker number  [default: 1883]
  -h, --host TEXT     Messaging host broker name  [default: 127.0.0.1]

 ```
 
It also provides a decorator `@signal_handler` for the definition of function handlers of POSIX signals:

```python
@signal_handler((Signals.SIGINT, Signals.SIGTERM))
def stop_handler(signum, _frame):
    logging.warning('Caught signal number ' + str(signum))
```

For a complete example, please look at the [simple-adapter](examples/simple-adapter/main.py).

### DeviceAdapter

The `DeviceAdapter` class inherits from `Adapter` and adds few methods
useful for adapters acting as a gateway for a specific technology such as
KNX or EnOcean.

It mainly subscribes to a `devicelist` topic and parses that list. Then 
it has another method to publish the measurements from these devices.

You could also use a function called `periodic`, an `asyncio` task which
will call the function you want periodically. It's a kind of timer using
cooperative multitasking.

Look at the [TABEDE](https://gitlab.csem.local/div-e/project/tabede) project for some examples.