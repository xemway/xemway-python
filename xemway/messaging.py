import paho.mqtt.client as mqtt
import os
from threading import Event
import uuid
import json
import yaml
import logging

VERSION_MAJOR = 2
VERSION_MINOR = 0


class subscribe:
    def __init__(self, topic, qos=0):
        self.topic = topic
        self.qos = qos

    def __call__(self, method):
        method.__topic__ = self.topic
        method.__qos__ = self.qos

        def get_wrapper(this):
            def wrapper(topic, message):
                return method(this, topic, message)

            return wrapper

        method._get_wrapper = get_wrapper
        return method


class query:
    def __init__(self, topic, qos=0):
        self.topic = topic + '/@query'
        self.qos = qos

    def __call__(self, method):
        method.__topic__ = self.topic
        method.__qos__ = self.qos

        def get_wrapper(this):
            def wrapper(topic, message):
                global response
                try:
                    requester_id = json.loads(message)['requester_id']
                    message_payload = json.loads(message)['payload']
                    version_major = json.loads(message)['version']['major']
                    command = message_payload['command']
                    message_body = message_payload['body']
                except json.decoder.JSONDecodeError as e:
                    print('Failed to decode JSON message: ' + str(e))  # TODO log
                except KeyError as e:
                    print('Failed to decode request, missing key: ' + str(e))  # TODO log
                else:
                    if version_major > VERSION_MAJOR:
                        error_response = dict()
                        error_response['error'] = 'Protocol version is not supported'
                        response = json.dumps(error_response)
                    else:
                        response = method(this, topic, command, message_body)

                    if response:
                        this.response(topic, requester_id, response)

            return wrapper

        method._get_wrapper = get_wrapper
        return method


class Messaging:
    def __init__(self, host='127.0.0.1', port=1883, username=None, password=None, credentials_file=None):
        self._subscriptions = set()
        self.host = host
        self.port = port
        self.cred = credentials_file
        self.username = username
        self.password = password
        self._responses = dict()
        self._connected_event = Event()
        self._is_connected = False

        self.client = mqtt.Client()

        logger = logging.getLogger(__name__)
        self.client.enable_logger(logger)

        # Attach subscriptions
        cls = self.__class__
        for attr_name in dir(cls):
            attr = getattr(cls, attr_name)
            if hasattr(attr, '__topic__'):
                self.subscribe(attr.__topic__, attr._get_wrapper(self))

    @classmethod
    def _read_subscriptions(cls):
        for attr in dir(cls):
            method = getattr(cls, attr)
            topic = getattr(method, '__messaging_topic__', None)
            if not hasattr(method, '__call__') or not topic:
                continue

            cls._subscriptions[topic] = method

    def connect(self, sync=False):
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

        if self.cred is not None:
            with open(self.cred, 'r') as yaml_file:
                cred_yaml = yaml.load(yaml_file, Loader=yaml.SafeLoader)
                username = cred_yaml['username']
                password = cred_yaml['password']
            self.client.username_pw_set(username, password)
        elif self.username is not None and self.password is not None:
            username = self.username
            password = self.password

        self.client.connect(self.host, self.port)

        if sync:
            self.client.loop_forever()
        else:
            self.client.loop_start()

        self._connected_event.wait(5)
        self._connected_event.clear()

        return self._is_connected

    def disconnect(self):
        self.client.loop_stop()
        self.client.disconnect()
        self._is_connected = False

    def publish(self, topic: str, message: str, retain=False):
        self.client.publish(topic, message, retain=retain)

    def delete_retain(self, topic: str):
        self.client.publish(topic, payload=None, retain=True)

    def subscribe(self, topic: str, callback):
        self._subscriptions.add((topic, callback,))

    def _subscribe(self, topic: str, callback):
        def on_message(_client, _userdata, msg):
            callback(msg.topic, msg.payload.decode('utf8'))

        self.client.message_callback_add(topic, on_message)
        self.client.subscribe(topic)

    def unsubscribe(self, topic):
        self.client.unsubscribe(topic)

    @staticmethod
    def _generate_json_request(requester_id: str, request: str):
        data = dict()
        data['version'] = dict()
        data['version']['major'] = VERSION_MAJOR
        data['version']['minor'] = VERSION_MINOR
        data['requester_id'] = requester_id
        data['payload'] = request
        return json.dumps(data)

    def request(self, topic: str, request, timeout=10):
        def resp_cb(answer_topic, payload):
            topic_head, requester_id = os.path.split(answer_topic)
            self._responses[requester_id]['payload'] = payload
            self._responses[requester_id]['event'].set()

        requester_id = str(uuid.uuid4())

        # Preparation for the response
        self._responses[requester_id] = dict()
        self._responses[requester_id]['event'] = Event()
        resp_topic = topic + '/' + requester_id
        self._subscribe(resp_topic, resp_cb)

        # Publish the query
        query_topic = topic + '/@query'
        json_data = self._generate_json_request(requester_id, request)
        self.publish(query_topic, json_data)

        # Wait for the answer
        answer = None
        ret = self._responses[requester_id]['event'].wait(timeout)
        if ret:
            answer = self._responses[requester_id]['payload']

        # Clean-up
        self.unsubscribe(resp_topic)
        del self._responses[requester_id]
        return answer

    def response(self, topic, requester_id, response_msg):
        topic_head, query_tail = os.path.split(topic)
        resp_topic = topic_head + '/' + requester_id
        self.publish(resp_topic, response_msg)

    def on_connect(self, _client, _userdata, _flags, rc):
        if rc == 0:
            for subscription in self._subscriptions:
                self._subscribe(*subscription)
            self._is_connected = True
        self._connected_event.set()

    def on_message(self, client, userdata, msg):
        pass
