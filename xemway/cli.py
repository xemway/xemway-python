import click


class command:
    def __init__(self, name=None, cls=click.Command, **attrs):
        self.name = name
        self.cls = cls
        self.attrs = attrs

    def __call__(self, method):
        def __command__(this):
            def wrapper(*args, **kwargs):
                return method(this, *args, **kwargs)

            return self.cls(self.name, callback=wrapper, **self.attrs)

        method.__command__ = __command__
        return method


class option:
    def __init__(self, *param_decls, **attrs):
        self.param_decls = param_decls
        self.attrs = attrs

    @staticmethod
    def is_class(arg):
        return isinstance(arg, type)

    def __call__(self, obj):
        if not hasattr(obj, '__click_options__'):
            obj.__click_options__ = []

        new_click_option = click.Option(param_decls=self.param_decls, **self.attrs)
        obj.__click_options__.append(new_click_option)

        return obj


class Cli:
    def __new__(cls, *args, **kwargs):
        self = super(Cli, cls).__new__(cls, *args, **kwargs)

        # Main command that dispatches options
        def cli(*args, **options):
            for global_option in self.__global_options__:
                if callable(global_option):
                    # This is a method decorator, let's call the linked method
                    global_option(self, **options)

            # Class options, let's update the option value with user value
            if hasattr(self, '__click_options__'):
                click_options_attr = getattr(cls, '__click_options__')
                for opt in click_options_attr:
                    setattr(self, opt.name, options[opt.name])

            self.main()

        self.cli = click.Group(callback=cli, invoke_without_command=True)

        # Wrap commands
        for attr_name in dir(cls):
            attr = getattr(cls, attr_name)
            if hasattr(attr, '__command__'):
                self.cli.add_command(attr.__command__(self))

        if not hasattr(self, '__global_options__'):
            self.__global_options__ = set()

        # Class options
        if hasattr(self, '__click_options__'):
            attr = getattr(cls, '__click_options__')
            self.cli.params.extend(attr)
            for new_option in attr:
                setattr(self, new_option.name, new_option.default)

        # Method options
        for attr_name in dir(cls):
            attr = getattr(cls, attr_name)
            if hasattr(attr, '__click_options__'):
                self.cli.params.extend(attr.__click_options__)
                self.__global_options__.add(attr)
        return self

    def main(self):
        pass

    def run(self):
        """Run the CLI application."""
        self()

    def __call__(self):
        """Run the CLI application."""
        self.cli()
