from enum import IntEnum
import signal
from .cli import Cli, option
from .messaging import Messaging
import json
import datetime
from collections import namedtuple
import asyncio
import time
from threading import Thread


class Signals(IntEnum):
    SIGABRT = 6
    SIGALRM = 14
    SIGBUS = 7
    SIGCHLD = 17
    SIGCLD = 17
    SIGCONT = 18
    SIGFPE = 8
    SIGHUP = 1
    SIGILL = 4
    SIGINT = 2
    SIGIO = 29
    SIGIOT = 6
    SIGKILL = 9
    SIGPIPE = 13
    SIGPOLL = 29
    SIGPROF = 27
    SIGPWR = 30
    SIGQUIT = 3
    SIGRTMAX = 64
    SIGRTMIN = 34
    SIGSEGV = 11
    SIGSTOP = 19
    SIGSYS = 31
    SIGTERM = 15
    SIGTRAP = 5
    SIGTSTP = 20
    SIGTTIN = 21
    SIGTTOU = 22
    SIGURG = 23
    SIGUSR1 = 10
    SIGUSR2 = 12
    SIGVTALRM = 26
    SIGWINCH = 28
    SIGXCPU = 24
    SIGXFSZ = 25


# Decorator function
def signal_handler(signal_numbers: list):
    def func_wrapper(function):
        for signal_number in signal_numbers:
            signal.signal(signal_number, function)
        return function

    return func_wrapper


async def periodic(period, loop, func, **kwargs):
    def gen_tick():  # Generator to avoid time drift
        t0 = time.time()
        count = 0
        while True:
            count += 1
            yield max(t0 + count * period - time.time(), 0)
    sleep_duration = gen_tick()
    while True:
        func(**kwargs)
        await asyncio.sleep(next(sleep_duration), loop=loop)


# The following option names were chosen so that it matches the Messaging property names
@option('-h', '--host', default='127.0.0.1', show_default=True, help='Messaging host broker name')
@option('-p', '--port', default=1883, show_default=True, help='Messaging port broker number')
@option('-c', '--cred', help='Messaging broker credentials YAML file')
class Adapter(Cli, Messaging):
    def __init__(self):
        Cli.__init__(self)
        Messaging.__init__(self)


def to_bool(value):
    if type(value) == bool:
        return value
    elif type(value) == str:
        if value.lower() == 'true' or value.lower() == 'on' or value == '1':
            return True
        else:
            return False
    else:
        return bool(value)


class DeviceAdapter(Adapter):
    TYPE_CONVERSION = {
        'temperature': float,
        'state': to_bool,
    }

    EndpointConfig = namedtuple(
        'EndpointConfig',
        'location device name type unit description address polling_period topic readable writable')

    def __init__(self, technology):
        super().__init__()
        self.technology = technology
        self._endpoints = []
        self._loop = asyncio.get_event_loop()
        self._thread = None

    def parse_device_info(self, devices_info, location):
        self._endpoints.clear()

        for device in devices_info:
            if device['technology'].lower() == self.technology:
                if 'outputs' in device:
                    for endpoint in device['outputs']:
                        new_endpoint = self.EndpointConfig(
                            location=location,
                            device=device['id'],
                            name=endpoint['name'],
                            type=endpoint['type'],
                            unit=endpoint['unit'] if 'unit' in endpoint else 'not defined',
                            description=device['description'],
                            address=device['address'],
                            polling_period=endpoint['period'] if 'period' in endpoint else 'not defined',
                            topic=endpoint['topic'],
                            readable=True if 'period' in endpoint else False,
                            writable=False,
                        )
                        self._endpoints.append(new_endpoint)
                if 'inputs' in device:
                    for endpoint in device['inputs']:
                        new_endpoint = self.EndpointConfig(
                            location=location,
                            device=device['id'],
                            name=endpoint['name'],
                            type=endpoint['type'],
                            unit=endpoint['unit'] if 'unit' in endpoint else 'not defined',
                            description=device['description'],
                            address=device['address'],
                            polling_period=endpoint['period'] if 'period' in endpoint else 'not defined',
                            topic=endpoint['topic'],
                            readable=True if 'period' in endpoint else False,
                            writable=True,
                        )
                        self._endpoints.append(new_endpoint)

    def stop_asyncio_tasks(self):
        for task in self._tasks:
            task.cancel()
        self._tasks.clear()

        if self._thread:
            self._loop.call_soon_threadsafe(self._loop.stop)
            self._thread.join()

    def restart_asyncio_tasks(self, periodic_handler):
        for index, endpoint in enumerate(self._endpoints):
            if endpoint.readable:
                new_task = asyncio.ensure_future(
                    periodic(
                        period=endpoint.polling_period,
                        loop=self._loop,
                        func=periodic_handler,
                        endpoint_index=index),
                    loop=self._loop)
                self._tasks.append(new_task)

        if self._tasks:
            # The asyncio loop runs in a separated thread so that the main thread is not blocked
            self._thread = Thread(target=self._loop.run_forever)
            self._thread.start()

    def publish_measurement(self, endpoint_index, new_value):
        iso_date_time = datetime.datetime.utcnow().replace(microsecond=0, tzinfo=datetime.timezone.utc).isoformat()
        iso_date_time = str(iso_date_time).replace('+00:00', 'Z')  # Use Zulu time representation for UTC time

        endpoint = self._endpoints[endpoint_index]

        message = dict()
        message['schema_version'] = 1
        message['measurement'] = dict()
        message['measurement']['device_id'] = endpoint.device
        message['measurement']['endpoint'] = endpoint.name
        message['measurement']['description'] = endpoint.description
        message['measurement']['location'] = endpoint.location
        message['measurement']['type'] = endpoint.type
        message['measurement']['unit'] = endpoint.unit
        if endpoint.type.lower() in self.TYPE_CONVERSION:
            message['measurement']['value'] = self.TYPE_CONVERSION[endpoint.type.lower()](new_value)
        else:  # Float conversion by default
            message['measurement']['value'] = float(new_value)
        message['measurement']['timestamp'] = iso_date_time

        json_message = json.dumps(message)
        self.publish(endpoint.topic, json_message, retain=True)
