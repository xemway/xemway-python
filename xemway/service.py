#!/usr/bin/python3
import os
import sys
import time
import click
import logging

from logging.handlers import SysLogHandler
from logging import StreamHandler

from daemonocle import Daemon

from .cli import Cli, option, command
from .messaging import Messaging


class Service(Cli):
    _name = None

    def __init__(self, name=None, description='Simple XEMWAY Service'):
        if name:
            self.name = name
        elif self._name:
            self.name = self._name
        else:
            self.name = self.__class__.__name__

        self.description = description

        self.logger = self._get_logger(name)

        self._debug = False

        self.daemon = Daemon(self.worker, shutdown_callback=self.shutdown)
        self.daemon.pidfile = '/var/run/xem/%s.pid' % self.name.lower()

        self._bind_daemon()

    def _get_command(self, action):
        def subcommand():
            self.daemon.do_action(action)
        return subcommand

    def _bind_daemon(self):
        """Daemonocle Daemon exposes methods to be used as cli commands.
        One binds these methods to the current instance.
        """
        for action in self.daemon.list_actions():
            subcommand = self._get_command(action)
            subcommand.__doc__ = self.daemon.get_action(action).__doc__
            self.cli.add_command(click.command(action)(subcommand))

    @option('-d', '--debug', is_flag=True, help='Do NOT detach and run in the background.')
    def debug(self, debug, **kwargs):
        self.daemon.detach = not debug

    @option('-i', '--pidfile', help="Set custom pidfile", metavar="<filename>")
    def pidfile(self, pidfile, **kwargs):
        if (pidfile):
            self.daemon.pidfile = pidfile
        else:
            self.daemon.pidfile = '/var/run/xem/%s.pid' % self.name.lower()

    def _get_logger(self, name):
        handler = StreamHandler()
        # handler = SysLogHandler('/dev/log', facility=SysLogHandler.LOG_DAEMON)

        logger = logging.getLogger(name)
        logger.setLevel(os.environ.get("LOGLEVEL", "DEBUG"))
        logger.addHandler(handler)

        return logger

    def worker(self):
        self.init()
        while True:
            self.logger.debug('Ten seconds have elapsed...')
            time.sleep(10)

    def init(self):
        """Called before the daemon starts."""
        self.logger.info('%s is starting' % self.name)

    def shutdown(self, message, code):
        """Called right before the daemon is stopped."""
        self.logger.info('%s is stopping' % self.name)
        if message:
            self.logger.debug(message)

    def get_service_file(self, binpath, unit={}, service={}):
        from configparser import ConfigParser
        from io import StringIO

        config = ConfigParser()

        config.add_section('Unit')
        config.set('Unit', 'SourcePath', os.path.dirname(binpath))
        config.set('Unit', 'Description', self.description)
        config.set('Unit', 'Before', 'multi-user.target')

        config.add_section('Service')
        config.set('Service', 'Type', 'simple')
        config.set('Service', 'PIDFile', self.daemon.pidfile)
        config.set('Service', 'ExecStart', '%s start' % binpath)
        config.set('Service', 'ExecStop', '%s stop' % binpath)
        config.set('Service', 'ExecReload', '%s reload' % binpath)

        for key, value in unit.items():
            config.set('Unit', key, value)

        for key, value in service.items():
            config.set('Unit', key, value)

        with StringIO() as fp:
            config.write(fp, space_around_delimiters=False)
            fp.seek(0)
            return fp.read()


class MessagingService(Service, Messaging):
    def __init__(self, host='127.0.0.1', port=1883):
        Service.__init__(self)
        Messaging.__init__(self, host, port)

    @option('-h', '--host', default='127.0.0.1', show_default=True)
    def _set_host(self, host, **kwargs):
        """Set hostname of the MQTT server."""
        self.host = host

    @option('-p', '--port', default=1883, show_default=True)
    def _set_port(self, port, **kwargs):
        """Set port number of the MQTT server."""
        self.port = port

    def worker(self):
        self.init()
        self.connect(sync=True)
