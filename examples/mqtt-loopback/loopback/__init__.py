from xemway import MessagingService, subscribe

PING = 'ping'
PONG = 'pong'


class Loopback(MessagingService):
    @subscribe(PING, qos=2)
    def foo(self, topic, payload):
        self.publish(PONG, payload)
