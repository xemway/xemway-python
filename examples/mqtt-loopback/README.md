# Loopback Demo

This small example shows how to use `xemway.service` to create a small daemon.

This daemon uses the `MessagingService`.

- It subscribes to a MQTT topic `ping`
- When it receives a new messages it publishes the same content on `pong`
