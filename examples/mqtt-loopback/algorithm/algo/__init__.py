from xemway import MessagingAlgorithmService


class Algorithm(MessagingAlgorithmService):
    _inputs_ = {
        'a': TelemetrySignal,
        'b': TelemetrySignal,
        'c': TelemetrySignal
    }
    _outputs_ = {
        'sum': TelemetrySignal
    }

    def compute(self):
        self.outputs.sum(
            sum(self.inputs.a) + sum(self.inputs.b) + sum(self.inputs.c))
