#!/usr/bin/env python3
from setuptools import setup

cli = 'xemway-demo-mqtt-loopback'

setup(
    name='xemway_mqtt_loopback',
    version='0.0.1',
    author="CHEVALLIER Yves",
    author_email="ycr@csem.ch",
    license='MIT',
    packages=[
        'loopback'
    ],
    include_package_data=True,
    install_requires=[
        'xemway@git+https://github.csem.local/xemway/xemway-python.git',
    ],
    entry_points={
        'console_scripts': [
            '%s = loopback.__main__:main' % cli,
        ],
    },
)
