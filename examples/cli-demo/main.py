from xemway import Cli, option, command


@option('-t', '--test', default=42, show_default=True, help='Answer to everything')
@option('-m', '--message', default='Hello', show_default=True, help='Greetings')
class CliDemo(Cli):
    def __init__(self):
        self.foo = 33
        self.bar = 66

    @command('ping')
    def ping(self):
        print('pong')

    @option('-f', '--foo', default=33, show_default=True, help='Decorator option test 1')
    def set_foo(self, foo, **kwargs):
        if foo:
            self.foo = foo

    @option('-b', '--bar', default=66, show_default=True, help='Decorator option test 2')
    def set_foo(self, bar, **kwargs):
        if bar:
            self.bar = bar

    def main(self):
        print(self.message)
        print(self.test)
        print(self.foo)
        print(self.bar)


if __name__ == '__main__':
    cli_demo = CliDemo()
    cli_demo()
