from xemway import Service, option, command

from time import sleep


class Talkative(Service):

    @option('-m', '--message', default='I am not yet sleeping!')
    def set_message(self, message, **kwargs):
        if message:
            self._message = message

    @command('shout')
    def shout(self):
        print("HEY!")

    def init(self):
        self.logger.info('I am going to talk too much...')

    def worker(self):
        while True:
            sleep(3)
            self.logger.info(self._message)

    def shutdown(self):
        self.logger.info('...Unfortunately, I am dying, you will not hear me anymore.')
