#!/usr/bin/env python3
from setuptools import setup

cli = 'xemway-demo-talkative'

setup(
    name='xemway_talkative',
    version='0.0.1',
    author="CHEVALLIER Yves",
    author_email="ycr@csem.ch",
    license='MIT',
    packages=[
        'talkative'
    ],
    include_package_data=True,
    install_requires=[
        'xemway@git+https://github.csem.local/xemway/xemway-python.git',
    ],
    entry_points={
        'console_scripts': [
            '%s = talkative.__main__:main' % cli,
        ],
    },
)
