#!/usr/bin/env python3

from sys import stdout
import time
from threading import Event
import logging
from xemway import Adapter, option, subscribe, query, Signals, signal_handler


@signal_handler((Signals.SIGINT, Signals.SIGTERM))
def stop_handler(signum, _frame):
    logging.warning('Caught signal number ' + str(signum))
    stop_event.set()
    time.sleep(0.5)


@option('-m', '--message', help='Just a Hello message', default='Hello World', show_default=True)
class SimpleAdapter(Adapter):
    def __init__(self):
        super().__init__()

    # Demonstrate pub/sub
    @subscribe('this/is/a/test/topic')
    def topic_handler(self, topic, payload):
        logging.info('topic: ' + topic + ', got message: ' + str(payload))

    # Demonstrate query/answer
    @query('this/is/a/test/request/handler')
    def request_handler(self, _topic, _payload):
        return 42

    def main(self):
        logging.info(self.message)

        success = self.connect()
        if not success:
            logging.error('Failed to connect to messaging service')
            return

        while not stop_event.wait(3):
            self.publish('this/is/a/test/topic', '1, 2, 3, do you hear me?', retain=True)
            time.sleep(0.1)
            response = self.request(
                'this/is/a/test/request/handler',
                'Ultimate Question of Life, the Universe, and Everything',
                timeout=2)
            logging.info('The Answer is ' + str(response))

        self.disconnect()
        logging.warning('Exiting main adapter function')


if __name__ == '__main__':
    # Set the logging level and format
    FORMAT = '%(asctime)-15s %(levelname)-8s %(message)s'
    logging.basicConfig(stream=stdout, level=logging.INFO, format=FORMAT)
    logging.info('starting...')

    stop_event = Event()
    simple_adapter = SimpleAdapter()
    simple_adapter()
