#!/usr/bin/env python3
from setuptools import setup

setup(
    name='xemway',
    version='1.0.3',
    use_scm_version=True,
    author="CHEVALLIER Yves / BANDERET Grégoire",
    author_email="gbt@csem.ch",
    description="Helpers for building Xemway applications",
    url="https://gitlab.csem.local/div-e/libraries/xemway/xemway-python.git",
    license='MIT',
    packages=[
        'xemway'
    ],
    include_package_data=True,
    install_requires=[
        'click~=8.1.7',
        'daemonocle~=1.2.3',
        'paho-mqtt~=1.6.1',
        'pyyaml~=6.0.1'
    ]
)
