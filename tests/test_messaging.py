import unittest
from unittest.mock import Mock
import time
import json
from xemway import Messaging, subscribe, query


# Prerequisite for the tests: A MQTT broker running at address BROKER_ADDR with user=doe, passwd=foo

BROKER_ADDR = '127.0.0.1'
BROKER_PORT = 1883


class TestHasMessaging(unittest.TestCase):
    def setUp(self):
        self._called = None

    def tearDown(self):
        self._called = None

    def test_01_connect_disconnect(self):
        messaging = Messaging(host=BROKER_ADDR, port=BROKER_PORT, credentials_file='test_credentials.yaml')
        ret = messaging.connect()
        self.assertTrue(ret)
        messaging.disconnect()

    def test_10_connect_with_wrong_user(self):
        messaging = Messaging(host=BROKER_ADDR, port=BROKER_PORT, credentials_file='test_credentials_wrong_user.yaml')
        ret = messaging.connect()
        self.assertFalse(ret)

    def test_11_connect_with_wrong_password(self):
        messaging = Messaging(host=BROKER_ADDR, port=BROKER_PORT, credentials_file='test_credentials_wrong_passwd.yaml')
        ret = messaging.connect()
        self.assertFalse(ret)

    def test_12_connect_with_no_user(self):
        messaging = Messaging(host=BROKER_ADDR, port=BROKER_PORT)
        ret = messaging.connect()
        self.assertFalse(ret)

    def test_20_pub_sub(self):
        messaging = Messaging(host=BROKER_ADDR, port=BROKER_PORT, credentials_file='test_credentials.yaml')
        topic1_callback = Mock()
        messaging.subscribe('xemway/testing/topic1', topic1_callback)
        ret = messaging.connect(sync=False)
        self.assertTrue(ret)
        messaging.publish('xemway/testing/topic1', 'Hello')
        time.sleep(1)  # Wait for callback execution
        topic1_callback.assert_called_once_with('xemway/testing/topic1', 'Hello')
        messaging.disconnect()

    def topic2a(self, topic, payload):
        self._called = payload
        self.assertEqual(topic, 'xemway/testing/topic2a')

    def test_22_pub_sub_unsubscribe(self):
        messaging = Messaging(host=BROKER_ADDR, port=BROKER_PORT, credentials_file='test_credentials.yaml')
        messaging.subscribe('xemway/testing/topic2a', self.topic2a)
        ret = messaging.connect(sync=False)
        self.assertTrue(ret)
        messaging.publish('xemway/testing/topic2a', 'Hello topic2a')
        time.sleep(1)  # Wait for callback execution
        self.assertEqual(self._called, 'Hello topic2a')
        self._called = None
        messaging.unsubscribe('xemway/testing/topic2a')
        time.sleep(1)
        messaging.publish('xemway/testing/topic2a', 'Hello topic2a')
        time.sleep(1)  # Wait for callback execution
        self.assertEqual(self._called, None)
        messaging.disconnect()

    def topic2b(self, topic, payload):
        self._called = payload
        self.assertEqual(topic, 'xemway/testing/topic2b')

    def test_30_pub_sub_retain(self):
        messaging = Messaging(host=BROKER_ADDR, port=BROKER_PORT, credentials_file='test_credentials.yaml')

        # Clean-up old retained message (if any from previous tests)
        ret = messaging.connect(sync=False)
        self.assertTrue(ret)
        messaging.delete_retain('xemway/testing/topic2b')
        time.sleep(1)  # Wait for callback execution
        self.assertEqual(self._called, None)
        messaging.disconnect()

        # Publish a message to be retained
        ret = messaging.connect(sync=False)
        self.assertTrue(ret)
        messaging.publish('xemway/testing/topic2b', 'Hello topic2b', retain=True)
        time.sleep(1)  # Wait for callback execution
        self.assertEqual(self._called, None)  # We are not subscribed
        messaging.disconnect()

        # Subscribe callback shall be called even without any call to publish
        messaging.subscribe('xemway/testing/topic2b', self.topic2b)
        ret = messaging.connect(sync=False)
        self.assertTrue(ret)
        time.sleep(1)  # Wait for callback execution
        self.assertEqual(self._called, 'Hello topic2b')
        self._called = None
        messaging.disconnect()

        # Test deletion of retained
        ret = messaging.connect(sync=False)
        self.assertTrue(ret)
        messaging.delete_retain('xemway/testing/topic2b')
        time.sleep(1)  # Wait for callback execution
        self.assertEqual(self._called, '')  # Not sure we want to be called for an empty publish (aka delete command)
        self._called = None
        messaging.unsubscribe('xemway/testing/topic2b')
        messaging.disconnect()
        # Test we have no more retained message on the topic
        messaging.subscribe('xemway/testing/topic2b', self.topic2b)
        ret = messaging.connect(sync=False)
        self.assertTrue(ret)
        time.sleep(1)  # Wait for callback execution
        self.assertEqual(self._called, None)
        messaging.disconnect()


class TestIsMessaging(Messaging, unittest.TestCase):
    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)
        Messaging.__init__(self, host=BROKER_ADDR, port=BROKER_PORT, credentials_file='test_credentials.yaml')
        self._topic2_request = 'Hello'
        self._topic3_request = 'ultimate answer'
        self._topic4_request = dict()
        self._topic4_request['command'] = 'GET'
        self._topic4_request['body'] = 'temperature'
        self._topic5a_request = dict()
        self._topic5a_request['version'] = dict()
        self._topic5a_request['version']['major'] = 2
        self._topic5a_request['version']['minor'] = 99
        self._topic5a_request['requester_id'] = '123456789-I-am-a-random-key-for-query-answer'
        self._topic5a_request['payload'] = dict()
        self._topic5a_request['payload']['command'] = 'Fake Command'
        self._topic5a_request['payload']['body'] = 'Fake request'
        self._topic5b_request = dict()
        self._topic5b_request['version'] = dict()
        self._topic5b_request['version']['major'] = 3
        self._topic5b_request['version']['minor'] = 4
        self._topic5b_request['requester_id'] = '123456789-I-am-a-random-key-for-query-answer'
        self._topic5b_request['payload'] = dict()
        self._topic5b_request['payload']['command'] = 'Fake Command'
        self._topic5b_request['payload']['body'] = 'Fake request'
        self._topic5c_request = dict()
        self._topic5c_request['version'] = dict()
        self._topic5c_request['version']['major'] = 2
        self._topic5c_request['version']['minor'] = 99
        self._topic5c_request['requester_id_wrong_key'] = '123456789-I-am-a-random-key-for-query-answer'
        self._topic5c_request['payload'] = dict()
        self._topic5c_request['payload']['command'] = 'Fake Command'
        self._topic5c_request['payload']['body'] = 'Fake request'
        self._topic6_request = dict()
        self._topic6_request['version'] = dict()
        self._topic6_request['version']['major'] = 1
        self._topic6_request['version']['minor'] = 99
        self._topic6_request['requester_id'] = '123456789-I-am-a-random-key-for-query-answer'
        self._topic6_request['payload'] = dict()
        self._topic6_request['payload']['command'] = 'Fake Command'
        self._topic6_request['payload']['body'] = 'Fake request'
        self._device_query_get = dict()
        self._device_query_get['command'] = 'GET'
        self._device_query_get['body'] = ''
        self._device_query_set = dict()
        self._device_query_set['command'] = 'SET'
        self._device_query_set['body'] = 1234

    def setUp(self):
        self._called = None

    def tearDown(self):
        self._called = None

    def test_01_connect_disconnect(self):
        self.connect()
        self.disconnect()

    @subscribe('xemway/testing/topic2')
    def topic2(self, _topic, payload):
        self._called = payload

    def test_10_subscriber_decorator(self):
        ret = self.connect(sync=False)
        self.assertTrue(ret)
        self.publish('xemway/testing/topic2', self._topic2_request)
        time.sleep(1)  # Wait for callback execution
        self.disconnect()
        self.assertEqual(self._called, self._topic2_request)

    def topic3_query(self, topic, message):
        requester_id = json.loads(message)['requester_id']
        self.assertEqual(json.loads(message)['payload'], self._topic3_request)
        time.sleep(2)  # For timeout testing
        self.response(topic, requester_id, '42')

    def test_20_request_response(self):
        self.subscribe('xemway/testing/topic3/@query', self.topic3_query)
        ret = self.connect(sync=False)
        self.assertTrue(ret)
        response = self.request('xemway/testing/topic3', self._topic3_request, timeout=4)
        self.assertEqual(response, '42')
        self.disconnect()

    def test_21_request_response_timeout(self):
        self.subscribe('xemway/testing/topic3/@query', self.topic3_query)
        ret = self.connect(sync=False)
        self.assertTrue(ret)
        response = self.request('xemway/testing/topic3', self._topic3_request, timeout=1)
        self.assertEqual(response, None)
        self.disconnect()

    @query('xemway/testing/topic4')
    def topic4(self, topic, command, message):
        self.assertEqual(topic, 'xemway/testing/topic4/@query')
        self.assertEqual(command, self._topic4_request['command'])
        self.assertEqual(message, self._topic4_request['body'])
        return '23'

    def test_30_query_decorator(self):
        ret = self.connect(sync=False)
        self.assertTrue(ret)
        response = self.request('xemway/testing/topic4', self._topic4_request, timeout=4)
        self.assertEqual(response, '23')
        self.disconnect()

    @query('xemway/testing/topic5')
    def topic5_query(self, _topic, _command, _message):
        return 'Fake answer'

    @query('xemway/testing/topic6')
    def topic6_query(self, _topic, _command, _message):
        pass  # If no return statement, no answer is sent

    @subscribe('xemway/testing/topic5/123456789-I-am-a-random-key-for-query-answer')
    def topic5_answer(self, _topic, message):
        self._called = message

    @subscribe('xemway/testing/topic6/123456789-I-am-a-random-key-for-query-answer')
    def topic6_answer(self, _topic, _message):
        self._called = 'I shall never be executed'

    def test_40_request_response_compatible_version(self):
        ret = self.connect(sync=False)
        self.assertTrue(ret)
        my_json_incompatible_request = json.dumps(self._topic5a_request)
        self.publish('xemway/testing/topic5/@query', my_json_incompatible_request)
        time.sleep(1)  # Wait for callback execution
        self.assertEqual(self._called, 'Fake answer')
        self.disconnect()

    def test_41_request_response_incompatible_version(self):
        ret = self.connect(sync=False)
        self.assertTrue(ret)
        my_json_incompatible_request = json.dumps(self._topic5b_request)
        self.publish('xemway/testing/topic5/@query', my_json_incompatible_request)
        time.sleep(1)  # Wait for callback execution
        answer_error = json.loads(self._called)
        self.assertEqual(answer_error['error'], 'Protocol version is not supported')
        self.disconnect()

    def test_45_request_response_no_response(self):
        ret = self.connect(sync=False)
        self.assertTrue(ret)
        self._called = None
        request = json.dumps(self._topic6_request)
        self.publish('xemway/testing/topic6/@query', request)
        time.sleep(1)  # Wait for callback execution
        self.assertEqual(self._called, None)
        self.disconnect()

    def test_50_malformed_request(self):
        ret = self.connect(sync=False)
        self.assertTrue(ret)
        my_json_malformed_request = '} malformed ;-( {'
        self.publish('xemway/testing/topic5/@query', my_json_malformed_request)
        time.sleep(1)  # Wait for callback execution
        self.assertEqual(self._called, None)
        self.disconnect()

    def test_51_missing_key_in_request(self):
        ret = self.connect(sync=False)
        self.assertTrue(ret)
        self.publish('xemway/testing/topic5/@query', json.dumps(self._topic5c_request))
        time.sleep(1)  # Wait for callback execution
        self.assertEqual(self._called, None)
        self.disconnect()

    @query('test_topic/+/b/command/get/+')
    def query_get_handler(self, topic, command, message_body):
        self.assertEqual(topic, 'test_topic/a/b/command/get/d/@query')
        self.assertEqual(command, 'GET')
        self.assertEqual(message_body, '')
        return 'Fake answer'

    def test_60_device_query_get_decorator(self):
        ret = self.connect(sync=False)
        self.assertTrue(ret)
        response = self.request('test_topic/a/b/command/get/d', self._device_query_get, timeout=4)
        self.assertEqual(response, 'Fake answer')
        self.disconnect()

    @query('test_topic/+/b/command/set/+')
    def query_set_handler(self, topic, command, message_body):
        self.assertEqual(topic, 'test_topic/a/b/command/set/d/@query')
        self.assertEqual(command, 'SET')
        self.assertEqual(message_body, 1234)
        return 6789

    def test_62_device_query_set_decorator(self):
        ret = self.connect(sync=False)
        self.assertTrue(ret)
        response = self.request('test_topic/a/b/command/set/d', self._device_query_set, timeout=4)
        self.assertEqual(response, '6789')
        self.disconnect()


if __name__ == '__main__':
    unittest.main()
